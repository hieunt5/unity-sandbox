using Core;
using TMPro;
using UnityEngine;

public class TestParams : MonoBehaviour
{
    private void Start()
    {
        Params.OnChanged.AddListener(UpdateText);
    }

    private void UpdateText()
    {
        GetComponent<TMP_Text>().text = Params.Instance.message;
    }
}
