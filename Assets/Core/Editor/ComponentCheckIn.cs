using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class ComponentCheckIn {
    static ComponentCheckIn()
    {
        ObjectFactory.componentWasAdded += OnComponentAdded;
    }

    private static void OnComponentAdded(Component obj)
    {
        Debug.Log($"{obj.GetType()} was added on {obj.gameObject.name}");
    }
}