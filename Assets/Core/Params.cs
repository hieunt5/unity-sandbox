using System;
using System.Threading.Tasks;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.RemoteConfig;
using UnityEngine;
using UnityEngine.Events;

namespace Core
{
    [ExecuteInEditMode]
    public class Params : MonoSingleton<Params>
    {
        public static UnityEvent OnChanged = new ();
        
        // TODO: params code generator
        #region PARAMS
        // UI - FEATURE - OTHER
        public string message;

        private void FetchParams()
        {
            message = RemoteConfigService.Instance.appConfig.GetString(nameof(message));
            Debug.Log($"Fetched message: {message}");
        }
        #endregion

        #region ATTRIBUTES
        // Optionally declare variables for any custom user attributes:
        public struct userAttributes { }
        
        // Optionally declare variables for any custom app attributes:
        public struct appAttributes { }
        
        // Optionally declare variables for attributes to filter on any of following parameters:
        public struct filterAttributes { }
        
        // Optionally declare a unique assignmentId if you need it for tracking:
        public string assignmentId;
        #endregion

        protected override void Awake()
        {
            base.Awake();
            
            InitializeRemoteConfigAsync();
        }

        // TODO: pull in edit mode
        [ContextMenu(nameof(Pull))]
        private void Pull()
        {
            InitializeRemoteConfigAsync();
        }
        
        private async Task InitializeRemoteConfigAsync()
        {
            // initialize handlers for unity game services
            await UnityServices.InitializeAsync();

            // options can be passed in the initializer, e.g if you want to set analytics-user-id or an environment-name use the lines from below:
            // var options = new InitializationOptions()
            //   .SetOption("com.unity.services.core.analytics-user-id", "my-user-id-1234")
            //   .SetOption("com.unity.services.core.environment-name", "production");
            // await UnityServices.InitializeAsync(options);

            // remote config requires authentication for managing environment information
            if (!AuthenticationService.Instance.IsSignedIn)
            {
                Debug.Log("SignInAnonymouslyAsync");
                await AuthenticationService.Instance.SignInAnonymouslyAsync();
            }
            
            Debug.Log($"Signed In {AuthenticationService.Instance.SessionTokenExists}");
            
            // Add a listener to apply settings when successfully retrieved:
            RemoteConfigService.Instance.FetchCompleted += ApplyRemoteSettings;

            // await RemoteConfigService.Instance.FetchConfigsAsync(new userAttributes(), new appAttributes(), new filterAttributes());
            await RemoteConfigService.Instance.FetchConfigsAsync(new userAttributes(), new appAttributes());
        }
        
        private void ApplyRemoteSettings (ConfigResponse configResponse) {
            // Conditionally update settings, depending on the response's origin:
            switch (configResponse.requestOrigin) {
                case ConfigOrigin.Default:
                    Debug.Log ("No settings loaded this session; using default values.");
                    break;
                case ConfigOrigin.Cached:
                    Debug.Log ("No settings loaded this session; using cached values from a previous session.");
                    break;
                case ConfigOrigin.Remote:
                    Debug.Log ("New settings loaded this session; update values accordingly.");
                    assignmentId = RemoteConfigService.Instance.appConfig.assignmentId;
                    FetchParams();
                    OnChanged?.Invoke();
                    break;
            }
        }

        private void OnDestroy()
        {
            OnChanged.RemoveAllListeners();
            // foreach(var d in OnChanged.GetInvocationList())
            // {
            //     OnChanged -= (Action)d;
            // }
        }
    }
}